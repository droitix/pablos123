 <header id="navigation">
            <div class="navbar" role="banner">  
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img class="main-logo img-responsive" src="images/logo1.png" alt="">
                        </a>
                    </div> 
                    <nav id="mainmenu" class="navbar-left collapse navbar-collapse"> 
                        <a class="secondary-logo" href="{{ url('/') }}">
                            <img class="img-responsive" src="images/logo.png" alt="">
                        </a>
                        <ul class="nav navbar-nav">                       
                            



<!--start the dropdown for each element-->
                            <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">VIP LOUNGE</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                     
                                                        
                                                         <h2>
                                                            <a href="{{ url('/venuehire') }}"><font color="black">VENUE HIRE</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="{{ url('/restaurant') }}"><font color="black">RESTAURANT</font></a>
                                                        </h2>
                                                        <hr>
                                                        <div class="entry-meta">
                                                                <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/14.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">HARARE LOUNGE</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail" >
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/24.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">VIC FALLS LOUNGE</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/16.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">GWERU LOUNGE</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <!--end of dropdown for each element-->

                                         <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">VISUAL STUDIO</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                        <h2>
                                                            <a href="{{ url('/videostudio') }}"></i><font color="black">VIDEO STUDIO</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="{{ url('/shows') }}"></i><font color="black">SHOWS</font></a>
                                                        </h2>
                                                        
                                                        <hr>
                                                        <div class="entry-meta">
                                                                <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <iframe width="640" height="360" src="https://www.youtube.com/embed/csg3s6Ob4uc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="{{ url('/shows') }}">Loud Africa Zimbabwe</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                          <iframe width="640" height="360" src="https://www.youtube.com/embed/6ql2hjFBsoo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="{{ url('/shows') }}">New competitions in the house</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                           <iframe width="640" height="360" src="https://www.youtube.com/embed/KoCh4dlzNPo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="{{ url('/shows') }}">Drama in music</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                           

                           <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">MUSIC STUDIO</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                        <h2>
                                                            <a href="{{ url('/music-studio') }}"><font color="black">RECORDING SERVICES</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="{{ url('/producer') }}"><font color="black">PRODUCER</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="{{ url('/producer') }}"><font color="black">RECORD LABEL</font></a>
                                                        </h2>
                                                        
                                                        <hr>
                                                        <div class="entry-meta">
                                                                <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="{{ url('/music-studio') }}"><img class="img-responsive" src="images/slider/17.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">

                                                            <a href="news-details.html">Recording Studio</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-header">
                                                            <a href="{{ url('/producer') }}"><img class="img-responsive" src="images/slider/18.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Mixing and Mastering</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="{{ url('/producer') }}"><img class="img-responsive" src="images/slider/20.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">Record label  audition</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>


                           <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">CELEBRITY NEWS</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                        <h2>
                                                            <a href="news-details.html"><font color="black">LATEST NEWS</font></a>
                                                        </h2>
                                                        <h2 >
                                                            <a href="news-details.html"><font color="black">FASHION</font></a>
                                                        </h2>
                                                       
                                                        <hr>
                                                        <div class="entry-meta">
                                                                <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/1.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html"></a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/post/pics/2.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html"></a>
                                                        </h2>
                                                    </div>

                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html"></a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                           <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">GALLERY</a></li>

                            <li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">ABOUT</a>
                                <div class="dropdown-menu mega-menu">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        
                                                    </div>
                                                    <div class="post-content">                              
                                                        
                                                        <h2>
                                                            <a href="news-details.html"><font color="black">PABLOZ PRODUCTIONS</font></a>
                                                        </h2>
                                                       
                                                        <hr>
                                                        <div class="entry-meta">
                                                               <ul class="list-inline">
                                                               <li class="publish-date"><i class="fab fa-facebook"></i>facebook</a>
                                                               </li>
                                                                <li class="views"><a href="#"><i class="fab fa-twitter"></i>twitter</a>
                                                                </li>
                                                                <li class="loves"><a href="#"><i class="fab fa-instagram"></i>instagram</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/12.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">In patnership with zambezi magic</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/14.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                            
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">We offer the best lounge service in town</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="post medium-post">
                                                    <div class="entry-header">
                                                        <div class="entry-thumbnail">
                                                            <a href="news-details.html"><img class="img-responsive" src="images/slider/1.jpg" alt="" /></a>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">                              
                                                        <div class="entry-meta">
                                                           
                                                        </div>
                                                        <h2 class="entry-title">
                                                            <a href="news-details.html">vip lounge</a>
                                                        </h2>
                                                    </div>
                                                </div><!--/post--> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>                   
                    </nav>
                    
                    <div class="searchNlogin">
                        <ul>
                            <li class="search-icon"><i class="fa fa-search"></i></li>
                            <li class="dropdown user-panel"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i></a>
                                <div class="dropdown-menu top-user-section">
                                    <div class="top-user-form">
                                        <form id="top-login" role="form">
                                            <div class="input-group" id="top-login-username">
                                                <span class="input-group-addon"><img src="images/others/user-icon.png" alt="" /></span>
                                                <input type="text" class="form-control" placeholder="Username" required="">
                                            </div>
                                            <div class="input-group" id="top-login-password">
                                                <span class="input-group-addon"><img src="images/others/password-icon.png" alt="" /></span>
                                                <input type="password" class="form-control" placeholder="Password" required="">
                                            </div>
                                            <div>
                                                <p class="reset-user">Forgot <a href="#">Password/Username?</a></p>
                                                <button class="btn btn-danger" type="submit">Login</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="create-account">
                                        <a href="#">Create a New Account</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="search">
                            <form role="form">
                                <input type="text" class="search-form" autocomplete="off" placeholder="Type & Press Enter">
                            </form>
                        </div> <!--/.search--> 
                    </div><!--.searchNlogin -->
                </div>  
            </div>
            
           </header>
        </header><!--/#navigation-->



