<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/music-studio', function () {
    return view('music-studio');
});

Route::get('/producer', function () {
    return view('producer');
});
Route::get('/videostudio', function () {
    return view('videostudio');
});
Route::get('/shows', function () {
    return view('shows');
});

Route::get('/restaurant', function () {
    return view('restaurant');
});

Route::get('/venuehire', function () {
    return view('venuehire');
});

