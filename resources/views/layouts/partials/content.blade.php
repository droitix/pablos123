 <div class="container-fluid">   
       
            <div class="section add inner-add">
                <a href="#"><img class="img-responsive" src="images/post/add/add2.jpg" alt="" /></a>
            </div><!--/.section-->      
            <div class="section">
                <div class="row">
                    <div class="col-sm-3">
                        <h2 class="section-title title" style="font-family: 'Fjalla One', sans-serif;">LATEST NEWS</h2> 
                        <div class="left-sidebar">
                            <div class="post medium-post">
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        <a href="news-details.html"><img class="img-responsive" src="images/slider/10.jpg" alt="" /></a>
                                    </div>
                                </div>
                                <div class="post-content">                              
                                    <div class="entry-meta">
                                        <ul class="list-inline">
                                            <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 13, 2018 </a></li>
                                            <li class="views"><a href="#"><i class="fa fa-eye"></i>200</a></li>
                                            <li class="loves"><a href="#"><i class="fa fa-heart"></i>372</a></li>
                                        </ul>
                                    </div>
                                    <h2 class="entry-title">
                                        <a href="news-details.html">Young artist releases album under Pabloz records</a>
                                    </h2>
                                </div>
                            </div><!--/post--> 
                            <div class="post medium-post">
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        <a href="news-details.html"><img class="img-responsive" src="images/slider/11.png" alt="" /></a>
                                    </div>
                                </div>
                                <div class="post-content">                              
                                    <div class="entry-meta">
                                        <ul class="list-inline">
                                            <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 11, 2018 </a></li>
                                            <li class="views"><a href="#"><i class="fa fa-eye"></i>21</a></li>
                                            <li class="loves"><a href="#"><i class="fa fa-heart"></i>372</a></li>
                                        </ul>
                                    </div>
                                    <h2 class="entry-title">
                                        <a href="news-details.html">Pablos productions sweeps all awards</a>
                                    </h2>
                                </div>
                            </div><!--/post--> 
                            <div class="post medium-post">
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        <a href="news-details.html"><img class="img-responsive" src="images/slider/1.jpg" alt="" /></a>
                                    </div>
                                </div>
                                <div class="post-content">                              
                                    <div class="entry-meta">
                                        <ul class="list-inline">
                                            <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Jan 5, 2016 </a></li>
                                            <li class="views"><a href="#"><i class="fa fa-eye"></i>21</a></li>
                                            <li class="loves"><a href="#"><i class="fa fa-heart-o"></i>372</a></li>
                                        </ul>
                                    </div>
                                    <h2 class="entry-title">
                                        <a href="news-details.html">Jamaican surtday dominates Harare...</a>
                                    </h2>
                                </div>
                            </div><!--/post--> 

                            <div class="post medium-post">
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        <a href="news-details.html"><img class="img-responsive" src="images/slider/4.jpg" alt="" /></a>
                                    </div>
                                    
                                </div>
                                <div class="post-content">                              
                                    <div class="entry-meta">
                                        <ul class="list-inline">
                                            <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Jan 5, 2016 </a></li>
                                            <li class="views"><a href="#"><i class="fa fa-eye"></i>21k</a></li>
                                            <li class="loves"><a href="#"><i class="fa fa-heart-o"></i>372</a></li>
                                        </ul>
                                    </div>
                                    <h2 class="entry-title">
                                        <a href="news-details.html">Much anticipated drama series of the year out!</a>
                                    </h2>
                                </div>
                            </div><!--/post--> 
                            
                          
                        </div><!--/left-sidebar-->  
                    </div>
                    
                    <div class="col-sm-6">
                        <div id="site-content" class="site-content">
                            <h1 class="section-title title" style="font-family: 'Fjalla One', sans-serif;"><a href="listing.html">EXCLUSIVE AND POPULAR CONTENTS</a></h1>
                            <div class="middle-content">
                                <div id="top-news" class="section top-news">
                                    <div class="post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                                <a href="news-details.html"><img class="img-responsive" src="images/slider/4.jpg" alt="" /></a>
                                            </div>
                                        </div>
                                        <div class="post-content">                              
                                            <div class="entry-meta">
                                              
                                            </div>
                                            <h2 class="entry-title" style="font-family: 'Fjalla One', sans-serif;">
                                                <a href="news-details.html">LOUD AFRICA SEASON 4 PREMIERE</a>
                                            </h2>
                                            <div class="entry-content">
                                                <p>Text of the printing and typesetting industry orem Ipsum has been the industry standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book ......</p>
                                            </div>
                                        </div>
                                    </div><!--/post--> 
                                    <div class="post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                                <a href="news-details.html"><img class="img-responsive" src="images/slider/5.jpg" alt="" /></a>
                                            </div>
                                        </div>
                                        <div class="post-content">                              
                                            <div class="entry-meta">
                                                
                                            </div>
                                            <h2 class="entry-title">
                                                <a href="news-details.html">PABLOZ FIVE SEASON 2 PREMIERE</a>
                                            </h2>
                                            <div class="entry-content">
                                                <p>Text of the printing and typesetting industry orem Ipsum has been the industry standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book ......</p>
                                            </div>
                                        </div>
                                    </div><!--/post--> 
                                    <div class="post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                                <a href="news-details.html"><img class="img-responsive" src="images/slider/2.jpg" alt="" /></a>
                                            </div>
                                        </div>
                                        <div class="post-content">                              
                                            <div class="entry-meta">
                                                
                                            </div>
                                            <h2 class="entry-title">
                                                <a href="news-details.html">MUSIC AUDITIONS BEHIND THE SCENCES </a>
                                            </h2>
                                            <div class="entry-content">
                                                <p>Text of the printing and typesetting industry orem Ipsum has been the industry standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book ......</p>
                                            </div>
                                        </div>
                                    </div><!--/post--> 
                                </div><!-- top-news -->
                                
                                <div class="section health-section">
                                    <h1 class="section-title"><a href="listing.html">LATEST IN MUSIC AND SHOWS</a></h1>
                                    <div class="health-feature">
                                        <div class="post">
                                            <div class="entry-header">
                                                <div class="entry-thumbnail">
                                                    <iframe width="640" height="360" src="https://www.youtube.com/embed/upUEP7uGmsg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"  allowfullscreen></iframe>
                                                </div>
                                            </div>
                                            <div class="post-content">                              
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 20, 2018 </a></li>
                                                        
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">Watch two upcoming artists rock the stage at the pabloz vic falls carnival</a>
                                                </h2>
                                            </div>
                                        </div><!--/post--> 
                                        <div class="post">
                                            <div class="entry-header">
                                                <div class="entry-thumbnail">
                                                    <a href="news-details.html"><img class="img-responsive" src="images/slider/3.jpg" alt="" /></a>
                                                </div>
                                            </div>
                                            <div class="post-content">                              
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 23, 2018 </a></li>
                                                        
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">It has never been easier to produce music </a>
                                                </h2>
                                            </div>
                                        </div><!--/post--> 
                                        <div class="post">
                                            <div class="entry-header">
                                                <div class="entry-thumbnail">
                                                    <a href="news-details.html"><img class="img-responsive" src="images/slider/12.jpg" alt="" /></a>
                                                </div>
                                            </div>
                                            <div class="post-content">                              
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 6, 2018 </a></li>
                                                        
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">All pabloz shows now airing on zambezi magic</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </div>
                                </div><!--/.health-section -->
                                
                                <div class="add inner-add">
                                   
                                </div><!--/.section-->
                              
                                
                       
                            </div><!--/.middle-content-->
                        </div><!--/#site-content-->
                    </div>
                    <div class="col-sm-3">
                        <div id="sitebar">                          
                            <div class="widget">
                                <h1 class="section-title title" style="font-family: 'Fjalla One', sans-serif;"><a href="listing.html">TRENDING</a></h1>
                           <a class="twitter-timeline" href="https://twitter.com/pablozvipclub?ref_src=twsrc%5Etfw">Tweets by pablozvipclub</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                            </div><!--/#widget-->
                            <div class="widget">
                                <h2 class="section-title">Upcoming Events</h2>
                                <ul class="comment-list">
                                    <li>
                                        <div class="post small-post">
                                            <div class="post-content">  
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="post-author"><a href="#">Event Date:</a></li>
                                                        <li class="publish-date"><a href="#">Nov 30, 2018 </a></li>
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">All white saturday at pabloz Harare</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                    <li>
                                        <div class="post small-post">
                                            <div class="post-content">  
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="post-author"><a href="#">Event Date:</a></li>
                                                        <li class="publish-date"><a href="#">Nov 28, 2018 </a></li>
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">Jamaican birthday bash</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                    <li>
                                        <div class="post small-post">
                                            <div class="post-content">  
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                       <li class="post-author"><a href="#">Event Date:</a></li>
                                                        <li class="publish-date"><a href="#">Nov 23, 2018 </a></li>
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">Premiere of pabloz 5 show</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                    <li>
                                        <div class="post small-post">
                                            <div class="post-content">
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="post-author"><a href="#">Event Date:</a></li>
                                                        <li class="publish-date"><a href="#">Dec 15, 2018 </a></li>
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">African cultural carnival</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                    <li>
                                        <div class="post small-post">
                                            <div class="post-content">
                                                <div class="entry-meta">
                                                    <ul class="list-inline">
                                                        <li class="post-author"><a href="#">Event Date:</a></li>
                                                        <li class="publish-date"><a href="#">Dec 24, 2018 </a></li>
                                                    </ul>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="news-details.html">Christmass count up Monday</a>
                                                </h2>
                                            </div>
                                        </div><!--/post-->
                                    </li>
                                </ul>
                            </div><!-- widget -->
                            
                            <div class="widget">
                                <div class="add">
                                    <a href="#"><img class="img-responsive" src="images/post/add/add5.jpg" alt="" /></a>
                                </div>
                            </div>
                            <div class="widget">
                                <div class="post video-post medium-post">
                                    <div class="entry-header">
                                        <div class="entry-thumbnail embed-responsive embed-responsive-16by9">
                                          <iframe width="640" height="360" src="https://www.youtube.com/embed/5HdcKZMiolo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    <div class="post-content">                              
                                        <div class="video-catagory"><a href="#">Music</a></div>
                                        <h2 class="entry-title">
                                            <a href="news-details.html">Loud Africa behind the scenes</a>
                                        </h2>
                                    </div>
                                </div><!--/post-->
                                
                            </div>
                        </div><!--/#sitebar-->
                    </div>
                </div>              
            </div><!--/.section-->

        </div><!--/.container-fluid-->