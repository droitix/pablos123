<!-- Full Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/slider/21.jpg');"></div>
                <div class="carousel-caption">
                    <div class="jumbotron" style="background:transparent">
  <h2>THE VIP LOUNGE,GWERU,HARARE<br> AND VIC FALLS</h2>
  <p>LETS GET GROOVING!</p>
  <p><a class="btn btn-primary btn-lg" href="#" role="button">EXCLUSIVE</a></p>
</div>
                </div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/slider/2.jpg');"></div>
                <div class="carousel-caption">
                    <div class="jumbotron" style="background:transparent">
  <h2>WATCH PABLOZ FIVE <br>PREMIERE THIS WEEK</h2>
  <p>ITS AIRING ON 25/12</p>
  <p><a class="btn btn-primary btn-lg" href="#" role="button">PREVIEW</a></p>
                </div>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/slider/3.jpg');"></div>
                <div class="carousel-caption">
                    <div class="jumbotron" style="background:transparent">
  <h2>LOUD AFRICA ZIMBABWE <br>SEASON FIVE </h2>
  <p>AIRING 22/12</p>
  <p><a class="btn btn-primary btn-lg" href="#" role="button">PREVIEW</a></p>
                </div>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </header>

   
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>




       

