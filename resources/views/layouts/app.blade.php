<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<head>
    @include('layouts.partials.head')

</head>
 

<body>

     <div id="main-wrapper"> 
   @include('layouts.partials.navigation')
    @include('layouts.partials.slider')

      @include('layouts.partials.content')
   
    </div>


     @include('layouts.partials.footer')

     <!--/#scripts--> 
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script> 
    <script type="text/javascript" src="js/main.js"></script>
</body>
 
</html>
